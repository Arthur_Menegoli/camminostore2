# Cammino Store 2

Project to update Cammino demo store

**Spree 4.0 powered by Ruby 2.7.0 on Rails 6.0.2.1**

* Ruby instalation
Requires RVM (curl -L https://get.rvm.io | bash -s stable)
```
rvm install ruby-2.7.0
rvm use ruby-2.7.0
```

* Spree instalation
access project's root
```
cd camminostore2/
```
run
```
bundle update
```

* Host server
```
bundle exec rails server
```

requires Node.js and Yarn

Rails webpacker
```
rails webpacker:install
```